'';

import React from 'react';

function PanelBody({ children, className }) {
  return (
    <div className={className ? className : 'card-block'}>
      {children}
    </div>
  );
}

module.exports = PanelBody;
