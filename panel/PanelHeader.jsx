'';

import React from 'react';

function PanelHeader({ children }) {
  return (
    <div className="card-header">
      {children}
    </div>
  );
}

module.exports = PanelHeader;
