'';

import React from 'react';

function PanelFooter({ children, className }) {
  return (
    <div className={className ? className : 'card-footer text-right'}>
      {children}
    </div>
  );
}

module.exports = PanelFooter;
