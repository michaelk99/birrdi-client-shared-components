'';

import React from 'react';

function PanelContainer({ children, className }) {
  const _className = className ? className : 'card card-shadow';
  return (
    <div className={_className}>
      {children}
    </div>
  );
}

module.exports = PanelContainer;
