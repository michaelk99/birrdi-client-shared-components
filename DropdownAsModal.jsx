import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside';
import classNames from 'classnames';

class DropdownAsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }
  handleClickOutside() {
    this.setState({
      isOpen: false
    });
  }
  render() {
    const {
      isOpen
    } = this.state;

    const {
      callbackOnClose,
      children,
      noIcon
    } = this.props;

    const dropdownClassName = classNames('dropdown', {
      'open': isOpen,
      'closed': !isOpen
    });

    const iconClassName = classNames({
      'dropdown-toggle-no-icon': noIcon,
      'dropdown-toggle': !noIcon
    });

    const dropdownMenuClassName = classNames('dropdown-menu', {
      [this.props.className]: this.props.className
    });

    return (
      <div
        className={dropdownClassName}
        onClick={() => {
          if(isOpen) {
            callbackOnClose(() => this.setState({ isOpen: !isOpen }));
            return;
          }
          this.setState({ isOpen: !isOpen });
        }}
      >
        <div className={iconClassName}>
          {this.props.customToggle ?
            this.props.customToggle :
            <span className="text-secondary">
              {this.props.toggleText}
            </span>
          }
        </div>
        <div className={dropdownMenuClassName}>
          {children}
        </div>
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(DropdownAsModal);
