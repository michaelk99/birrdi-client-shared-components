import React from 'react';

class Collapse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  render() {
    const {
      children,
      component
    } = this.props;

    const {
      isOpen
    } = this.state;

    return (
      <div className="">
        <div
          onClick={() => this.setState({ isOpen: !isOpen })}
        >
          {component}
        </div>
        {isOpen ?
          <div>
            {children}
          </div> :
          null
        }
      </div>
    );
  }
}

module.exports = Collapse;
