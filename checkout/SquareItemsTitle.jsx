import React from 'react';

function SquareItemsTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Square items:</div>
    </div>
  );
}

module.exports = SquareItemsTitle;
