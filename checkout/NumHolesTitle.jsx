import React from 'react';

function NumHolesTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Choose holes:</div>
    </div>
  );
}

module.exports = NumHolesTitle;
