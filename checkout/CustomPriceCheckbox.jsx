import React from 'react';

import classNames from 'classnames';

function CustomPriceCheckbox(props) {
  const {
    isCustomPrice,
    updateCheckoutForm
  } = props;

  const copy = isCustomPrice ? 'Cancel' : 'Change';
  const divClassName = classNames('d-inline cursor', {
    'm-l-1': !isCustomPrice
  });

  return (
    <div
      className={divClassName}
      onClick={() => {
        updateCheckoutForm({
          key: 'isCustomPrice',
          value: !isCustomPrice
        });
      }}
    >
      <small className="text-primary c-underline">
        {copy}
      </small>
    </div>
  );
}

module.exports = CustomPriceCheckbox;
