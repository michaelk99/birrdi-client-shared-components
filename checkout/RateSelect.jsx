import React from 'react';

import Select from '@shared/elements/Select';

import {
  GOLFER_TYPES
} from 'constants/user';

const labelMap = {
  [GOLFER_TYPES.NON_RESIDENTS]: 'Non resident',
  [GOLFER_TYPES.RESIDENTS]: 'Resident',
  [GOLFER_TYPES.SENIORS]: 'Senior',
  [GOLFER_TYPES.JUNIORS]: 'Junior',
  [GOLFER_TYPES.STUDENTS]: 'Student'
};

function RateSelect(props) {
  const { rateTypes, currentRate, setCurrentRate } = props;

  if(rateTypes.length === 0) {
    return null;
  }

  const options = rateTypes.map((rt, i) => {
    return <option key={i} value={rt}>{labelMap[rt]}</option>;
  });

  return (
    <div className="c-flex-child">
      <Select
        className="c-select"
        onChange={e => setCurrentRate(e.target.value)}
        value={currentRate}
      >
        {options}
      </Select>
      {currentRate.message &&
        <div className="m-t-2">{currentRate.message}</div>
      }
    </div>
  );
}

module.exports = RateSelect;
