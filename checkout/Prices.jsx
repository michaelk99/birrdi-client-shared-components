import React from 'react';

import classNames from 'classnames';

import CustomDropdown from '@shared/customDropdown/CustomDropdown';
import CustomDropdownOption from '@shared/customDropdown/CustomDropdownOption';

import {
  getPrice
} from '@utils/calculatePrice';

function Prices(props) {
  const {
    addMyPrice,
    className,
    displayText,
    myPrices,
    numHoles,
    prices,
    removeMyPrice,
    teeTime
  } = props;

  const priceClassName = classNames({
    'c-flex-child': !className,
    [className]: className
  });

  const {
    // customPrice is also connected to the tee time
    customPrice,
    // group size minimum
    numMinPlayers
  } = teeTime;

  // custom price will always have a constant display name golfer
  if(customPrice) {
    return (
      <div className={priceClassName}>
        <CustomDropdown displayText={displayText}>
          <CustomDropdownOption
            amount={customPrice}
            name="Golfer"
            numSelected={myPrices.length}
            onAdd={option => addMyPrice(option)}
            onRemove={option => removeMyPrice(option)}
            option={{
              id: -1,
              priceId: null,
              displayName: 'Golfer',
              amount: customPrice
            }}
          />
        </CustomDropdown>
        {numMinPlayers != 1 ?
          <small>{numMinPlayers} group minimum</small> :
          null
        }
      </div>
    );
  }

  return (
    <div className={priceClassName}>
      <CustomDropdown displayText={displayText}>
        {prices.map(price => {
          // util to resolve correct price
          const amount = getPrice({
            teeTime,
            price,
            numHoles
          });

          const name = price.name;

          // num times selected for this price
          const numSelected = myPrices.filter(m => m.id === price.id).length;

          return (
            <CustomDropdownOption
              amount={amount}
              key={`price-dropdown-${price.id}-${amount}`}
              name={name}
              numSelected={numSelected}
              onAdd={option => addMyPrice(option)}
              onRemove={option => removeMyPrice(option)}
              option={price}
            />
          );
        })}
      </CustomDropdown>
      {numMinPlayers != 1 ?
        <small>{numMinPlayers} group minimum</small> :
        null
      }
    </div>
  );
}

module.exports = Prices;
