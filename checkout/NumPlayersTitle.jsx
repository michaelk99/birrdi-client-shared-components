import React from 'react';

function NumPlayersTitle(props) {
  const { teeTime } = props;
  const { numSpotsLeft } = teeTime;

  return (
    <div className="c-flex-child">
      <div className="hidden-xs-down">
        <div>Number of players:</div>
        <small className="d-block">
          {numSpotsLeft + ' spots left'}
        </small>
      </div>
      <div className="hidden-sm-up">
        <div># players:</div>
        <small className="d-block">
          {numSpotsLeft + ' spots left'}
        </small>
      </div>
    </div>
  );
}

module.exports = NumPlayersTitle;
