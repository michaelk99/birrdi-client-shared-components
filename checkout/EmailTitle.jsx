import React from 'react';

function EmailTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Email:*</div>
    </div>
  );
}

module.exports = EmailTitle;
