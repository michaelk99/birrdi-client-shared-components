import React from 'react';

function GolfCourse() {
  return (
    <div className="c-flex-child">
      <div className="hidden-xs-down">
          <div>Golf Course:</div>
      </div>
      <div className="hidden-sm-up">
          <div>Course:</div>
      </div>
    </div>
  );
}

module.exports  = GolfCourse;
