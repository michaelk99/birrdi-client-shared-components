import React from 'react';

import moment from 'moment';

import Modal from '@shared/modals/Modal';
import BtnDefault from '@shared/elements/BtnDefault';
import FormattedAccessory from '@shared/utils/FormattedAccessory';
import FormattedPrice from '@shared/utils/FormattedPrice';

import pointOfSale from '@utils/pointOfSale';

import CustomFormContainer from '@shared/form/CustomFormContainer';
import CustomFormBody from '@shared/form/CustomFormBody';
import CustomFormHeader from '@shared/form/CustomFormHeader';

function ReceiptWithSquare(props) {
  const {
    customPrice,
    isCustomPrice,
    onClose,
    userTeeTime
  } = props;

  const {
    golfCourseName,
    startDate,
    startTime,
    totalPrice,
    userAccessories,
    userPrices
  } = userTeeTime;

  const headerTitle = 'Receipt';

  return (
    <Modal
      initializeAsOpen
      onCloseClick={() => {
        onClose();
        window.location.href = '/app';
      }}
      positionAsAbsolute
    >
      <div className="m-t-3">
        <CustomFormContainer>
          <CustomFormHeader>
            <div className="text-center">
              <div>
                <h3>
                  {headerTitle}
                  <i className="fas fa-check-circle m-l-1"></i>
                </h3>
              </div>
              <div className="m-t-1 text-primary">
                <h1>
                  {moment(startTime, 'HH:mm:ss').format('h:mm a')}
                </h1>
              </div>
              <div className="m-t-1">
                {golfCourseName}
              </div>
              <div>
                <small>
                  {moment(startDate).format('MMM Do, YYYY')}
                </small>
              </div>
            </div>
          </CustomFormHeader>
          <CustomFormBody>
            <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
              <div className="c-flex-child text-primary hidden-xs-down">
                <span>Reservation:</span>
              </div>
              <div className="c-flex-child">
                <div>{`$${totalPrice}`}</div>
              </div>
            </div>
            <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
              <div className="c-flex-child text-primary hidden-xs-down">
                <span>Players:</span>
              </div>
              <div className="c-flex-child">
                <FormattedPrice
                  userPrices={userPrices}
                />
              </div>
            </div>
            <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
              <div className="c-flex-child text-primary hidden-xs-down">
                <span>Additional items:</span>
              </div>
              <div className="c-flex-child">
                <FormattedAccessory
                  userAccessories={userAccessories}
                />
              </div>
            </div>
            <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
              <div className="c-flex-child text-primary hidden-xs-down">
                <span>Number of holes:</span>
              </div>
              <div className="c-flex-child">
                <div>{`${userTeeTime.numHoles} holes`}</div>
              </div>
            </div>
            <div className="m-t-2">
              <BtnDefault
                className="btn-block"
                onClick={() => {
                  // this will open up the square point of sale
                  // app
                  const url = pointOfSale({
                    totalAmount: (
                      isCustomPrice ?
                      customPrice :
                      totalPrice
                    ),
                    state: {
                      userTeeTime
                    }
                  });
                  window.location.href = url;
                }}
              >
                Checkout with Square
              </BtnDefault>
            </div>
          </CustomFormBody>
        </CustomFormContainer>
      </div>
    </Modal>
  );
}

module.exports = ReceiptWithSquare;
