import React from 'react';

import find from 'lodash/find';
import range from 'lodash/range';

import Select from '@shared/elements/Select';

import {
  PRICE_EXTRAS
} from 'constants/user';

const MAX_NUM_CARTS = 2; // Add 1 to account for off by 0

function GolfCartSelect(props) {
  const { teeTime, priceExtras, numGolfCarts, setNumGolfCarts } = props;
  const { numGolfCartsBooked, numSpotsLeft } = teeTime;

  const priceExtra = find(priceExtras, {
    type: PRICE_EXTRAS.GOLF_CART
  });

  if(!priceExtra) {
    return null;
  }

  let numCartsLeft = MAX_NUM_CARTS - numGolfCartsBooked;
  if(numSpotsLeft < 2 && numCartsLeft === 2) {
    numCartsLeft = 1;
  }

  let options = <option disabled value="0">No carts available</option>;
  if(numCartsLeft !== 0) {
    options = [];
    range(0, numCartsLeft + 1).map((__, i) => {
      options.push(<option key={i} value={i}>{i}</option>);
    });
  }

  return (
    <div className="c-flex-child">
      <Select
        className="c-select"
        onChange={e => setNumGolfCarts(e.target.value)}
        value={numGolfCarts || 0}
      >
        {options}
      </Select>
    </div>
  );
}

module.exports = GolfCartSelect;
