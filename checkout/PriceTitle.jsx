import React from 'react';

function PriceTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Choose golfers:</div>
    </div>
  );
}

module.exports = PriceTitle;
