import React from 'react';

function AccessoryTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Choose other items:</div>
    </div>
  );
}

module.exports = AccessoryTitle;
