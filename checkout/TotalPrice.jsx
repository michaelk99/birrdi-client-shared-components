import React from 'react';

import classNames from 'classnames';

function TotalPrice(props) {
  const {
    hidePaymentCopy,
    isAdmin,
    isOnBookingTab,
    shouldIncludeGolfCartInPrice,
    totalPrice
  } = props;

  return (
    <div>
      <div className="text-bold">
        {`$${totalPrice}`}
      </div>
      {!isAdmin && shouldIncludeGolfCartInPrice ?
        <div
          style={{
            marginTop: '.5rem'
          }}
        >
          <div>
            <small>
              <i>Golf cart included</i>
            </small>
          </div>
        </div> :
        null
      }
      {!isAdmin && !isOnBookingTab && !hidePaymentCopy ?
        <div
          style={{
            marginTop: shouldIncludeGolfCartInPrice ? '' : '.5rem'
          }}
        >
          <div>
            <small>
              <i>
                Pay at the golf course
              </i>
            </small>
          </div>
        </div> :
        null
      }
    </div>
  );
}

module.exports  = TotalPrice;
