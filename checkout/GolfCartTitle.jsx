import React from 'react';

import find from 'lodash/find';

import { PRICE_EXTRAS } from 'constants/user';

function GolfCartTitle(props) {
  const { priceExtras } = props;

  const priceExtra = find(priceExtras, {
    type: PRICE_EXTRAS.GOLF_CART
  });

  if(!priceExtra) {
    return null;
  }

  return (
    <div className="c-flex-child">
      <div className="hidden-xs-down">
        <div>Number of golf carts:</div>
        <small className="d-block">
          {`$${priceExtra.amount}/cart`}
        </small>
      </div>
      <div className="hidden-sm-up">
        <div># golf carts:</div>
        <small className="d-block">
          {`$${priceExtra.amount}/cart`}
        </small>
      </div>
    </div>
  );
}

module.exports = GolfCartTitle;
