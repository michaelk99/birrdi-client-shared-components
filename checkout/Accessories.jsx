import React from 'react';

import classNames from 'classnames';

import CustomDropdown from '@shared/customDropdown/CustomDropdown';
import CustomDropdownOption from '@shared/customDropdown/CustomDropdownOption';

function Accessories(props) {
  const {
    accessories,
    addMyAccessory,
    className,
    displayText,
    myAccessories,
    numHoles,
    removeMyAccessory
  } = props;

  const accessoryClassName = classNames({
    'c-flex-child': !className,
    [className]: className
  });

  // this would be an issue, so protect against nulls
  if(!accessories.length) {
    return null;
  }

  return (
    <div className={accessoryClassName}>
      <CustomDropdown displayText={displayText}>
        {accessories.map(golfCourseAccessory => {
          // how many of these accessories has the user added
          const numSelected = (
            myAccessories
            .filter(m => m && m.id === golfCourseAccessory.id)
            .length
          );

          // an accessory might have a nine hole amount so we check that here
          let amount = golfCourseAccessory.amount;

          // this needs to be == and not ===. This bug was rough.
          if(numHoles == 9 && golfCourseAccessory.nineHoleAmount != null) {
            amount = golfCourseAccessory.nineHoleAmount;
          }

          return (
            <CustomDropdownOption
              amount={amount}
              key={`golfcourse-accessory-${golfCourseAccessory.id}-${amount}`}
              name={golfCourseAccessory.name}
              numSelected={numSelected}
              onAdd={() => {
                addMyAccessory({
                  golfCourseAccessory,
                  key: numHoles === 18 ? 'amount' : 'nineHoleAmount',
                  value: amount
                });
              }}
              onRemove={option => removeMyAccessory(option)}
              option={golfCourseAccessory}
            />
          );
        })}
      </CustomDropdown>
    </div>
  );
}

module.exports = Accessories;
