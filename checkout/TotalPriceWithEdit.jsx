import React from 'react';
import CustomPriceCheckbox from './CustomPriceCheckbox';

function TotalPriceWithEdit(props) {
  const {
    customPrice,
    isCustomPrice,
    totalPrice,
    totalPriceBeforeTax,
    tax,
    updateCheckoutForm
  } = props;

  if(isCustomPrice) {
    return (
      <div className="c-flex-child">
        <div className="d-flex">
          <input
            onChange={e => updateCheckoutForm({
              key: 'customPrice',
              value: e.target.value
            })}
            placeholder="10.75"
            style={{
              width: '110px',
              marginRight: '.75rem'
            }}
            value={customPrice}
          />
          <CustomPriceCheckbox
            isCustomPrice={isCustomPrice}
            updateCheckoutForm={updateCheckoutForm}
          />
        </div>
      </div>
    );
  }

  return (
    <div className="c-flex-child">
      {tax ?
        <div>
          <div>
            {`Tee Time: $${totalPriceBeforeTax}`}
          </div>
        </div> :
        null
      }
      {tax ?
        <div>
          <div>
            {`Tax: $${tax}`}
          </div>
        </div> :
        null
      }
      <div className={tax ? 'm-t-1' : ''}>
        <span className="text-primary f5">
          ${totalPrice}
        </span>
        <CustomPriceCheckbox
          isCustomPrice={isCustomPrice}
          updateCheckoutForm={updateCheckoutForm}
        />
      </div>
    </div>
  );
}

module.exports  = TotalPriceWithEdit;
