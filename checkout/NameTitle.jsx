import React from 'react';

function NameTitle() {
  return (
    <div className="c-flex-child hidden-xs-down">
      <div className="text-italic text-primary">Name:*</div>
    </div>
  );
}

module.exports = NameTitle;
