import React from 'react';

import throttle from 'lodash/throttle';

import BtnDefault from '@shared/elements/BtnDefault';

class ReserveBtn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitting: false
    };
  }
  render() {
    const {
      myPrices,
      reserveTeeTime
    } = this.props;

    const {
      isSubmitting
    } = this.state;

    if(!myPrices.length) {
      return (
        <div className="m-t-1">
          <BtnDefault
            className="btn-block"
            disabled
          >
            <div>No golfers selected yet</div>
          </BtnDefault>
        </div>
      );
    }

    return (
      <div className="m-t-1">
        <BtnDefault
          className="btn-block"
          isLoading={isSubmitting}
          onClick={() => {
            this.setState({
              isSubmitting: true
            });
            // use throttle to prevent admin from hitting button multiple
            // times
            throttle(reserveTeeTime, 3000)();

            // always reset isSubmitting back to false so we don't land in
            // an infinite loading gif
            // give a 2s grace period
            setTimeout(() => {
              this.setState({
                isSubmitting: false
              });
            }, 2000);
          }}
        >
          <div>Reserve now</div>
        </BtnDefault>
      </div>
    );
  }
}

module.exports  = ReserveBtn;
