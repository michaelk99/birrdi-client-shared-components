import React from 'react';

import Select from '@shared/elements/Select';

import range from 'lodash/range';

function NumPlayersSelect(props) {
  const { teeTime, numPlayers, setNumPlayers } = props;
  const { numSpotsLeft } = teeTime;

  const options = range(numSpotsLeft).map((__, i) => {
    return <option key={i} value={i + 1}>{i + 1}</option>;
  });

  return (
    <div className="c-flex-child">
      <Select
        className="c-select"
        onChange={e => setNumPlayers(e.target.value)}
        value={numPlayers || 1}
      >
        {options}
      </Select>
    </div>
  );
}

module.exports = NumPlayersSelect;
