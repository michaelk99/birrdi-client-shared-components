import React from 'react';

import classNames from 'classnames';
import Select from 'react-select';

function NumHoles(props) {
  const {
    className,
    myPrices,
    numHoles,
    setNumHoles,
    teeTime
  } = props;

  const holeClassName = classNames('c-flex-child', {
    [className]: className
  });

  // only allow 9 holes for this tee time
  if(teeTime.numHoles === 9) {
    return (
      <Select
        clearable={false}
        options={[{
          value: 9,
          label: 9
        }]}
        styles={{
          option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor: 'rgba(0, 113, 230, 0.08)',
              color: '#262626'
            };
          }
        }}
        value={{
          value: 9,
          label: 9
        }}
      />
    );
  }

  const canHaveNineHoles = myPrices.some(p => {
    return p.nineHoleAmount || p.nineHoleTwilightAmount;
  });

  // if price selected has a 9 hole amount, allow the option for choosing
  // 9 or 18 holes
  if(canHaveNineHoles) {
    return (
      <Select
        clearable={false}
        options={[{
          value: 9,
          label: 9
        }, {
          value: 18,
          label: 18
        }]}
        value={numHoles}
        styles={{
          option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
              ...styles,
              backgroundColor: 'rgba(0, 113, 230, 0.08)',
              color: '#262626'
            };
          }
        }}
      />
    );
  }

  // otherwise just show 18 holes
  return (
    <Select
      options={[{
        value: 18,
        label: 18
      }]}
      value={{
        value: 18,
        label: 18
      }}
      styles={{
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
          return {
            ...styles,
            backgroundColor: 'rgba(0, 113, 230, 0.08)',
            color: '#262626'
          };
        }
      }}
    />
  );
}

module.exports = NumHoles;
