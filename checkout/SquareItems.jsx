import React from 'react';

import classNames from 'classnames';

import CustomDropdown from '@shared/customDropdown/CustomDropdown';
import CustomDropdownOption from '@shared/customDropdown/CustomDropdownOption';

function SquareItems(props) {
  const {
    addMySquareItem,
    className,
    displayText,
    isLoading,
    items,
    mySquareItems,
    removeMySquareItem
  } = props;

  const squareClassName = classNames({
    'c-flex-child': !className,
    [className]: className
  });

  if(isLoading) {
    return (
      <div className={squareClassName}>Loading ...</div>
    );
  }

  if(!items.length) {
    return (
      <div className={squareClassName}>No square items</div>
    );
  }

  return (
    <div className={squareClassName}>
      <CustomDropdown displayText={displayText}>
        {items.map(item => {
          const {
            itemData
          } = item;

          const {
            labelColor,
            name
          } = itemData;

          // variation needs to be divided by 100
          const amount = (
            itemData.variations[0].itemVariationData.priceMoney.amount / 100
          );

          const numSelected = mySquareItems.filter(m => m.id == item.id).length;

          return (
            <CustomDropdownOption
              amount={amount}
              key={`item-dropdown-${item.id}`}
              name={name}
              numSelected={numSelected}
              onAdd={option => addMySquareItem(option)}
              onRemove={option => removeMySquareItem(option)}
              option={item}
            />
          );
        })}
      </CustomDropdown>
    </div>
  );
}

module.exports = SquareItems;
