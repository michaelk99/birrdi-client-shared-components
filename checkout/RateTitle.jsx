import React from 'react';

import { GOLFER_TYPES } from 'constants/user';

const labelMap = {
  [GOLFER_TYPES.NON_RESIDENTS]: 'Non resident',
  [GOLFER_TYPES.RESIDENTS]: 'Resident',
  [GOLFER_TYPES.SENIORS]: 'Senior',
  [GOLFER_TYPES.JUNIORS]: 'Junior',
  [GOLFER_TYPES.STUDENTS]: 'Student'
};

function RateTitle(props) {
  const { rateTypes } = props;

  if(rateTypes.length === 0) {
    return null;
  }

  const ratesAsString =  rateTypes.map(rt => labelMap[rt]).join(', ');

  return (
    <div className="c-flex-child">
      <div>Rate:</div>
      <small>
        {ratesAsString + ' rate(s)'}
      </small>
    </div>
  );
}

module.exports = RateTitle;
