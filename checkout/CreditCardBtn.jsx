import React from 'react';

import Input from '@shared/elements/Input';
import BtnDefault from '@shared/elements/BtnDefault';

function CreditCardBtn(props) {
  const { totalPrice, myPrices } = props;

  if(totalPrice == 0 || totalPrice == '0.00' || myPrices.length === 0) {
    return (
      <form
        className="form-group"
        id="checkout-form"
      >
        <div id="dropin-container"></div>
        <div className="m-t-1">
          <BtnDefault className="btn-block" disabled>
            <div>No golfers selected yet</div>
          </BtnDefault>
        </div>
      </form>
    );
  }

  return (
    <form
      className="form-group"
      id="checkout-form"
    >
      <div id="dropin-container"></div>
      <Input
        className="btn btn-block btn-default-outline m-t-1"
        type="submit"
        value={`Charge $${totalPrice}`}
      />
    </form>
  );
}

module.exports  = CreditCardBtn;
