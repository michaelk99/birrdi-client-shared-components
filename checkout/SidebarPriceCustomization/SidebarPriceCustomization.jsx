import React from 'react';

import enhanceWithClickOutside from 'react-click-outside';

import BtnDefault from '@shared/elements/BtnDefault';
import Prices from '@shared/checkout/Prices';
import Accessories from '@shared/checkout/Accessories';
import TotalPrice from '@shared/checkout/TotalPrice';
import SquarePosIcon from '@shared/icons/SquarePosIcon';
import NumHoles from '@shared/checkout/NumHoles';
import SquareItems from '@shared/checkout/SquareItems';

import pointOfSale from '@utils/pointOfSale';

class SidebarPriceCustomization extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.state = {
      isOpen: false
    };
  }
  handleClickOutside() {
    this.setState({
      isOpen: false
    });
  }
  render() {
    const {
      isOpen
    } = this.state;

    const {
      addMyAccessory,
      addMyPrice,
      addMySquareItem,
      closeSidebar,
      configureCheckoutFromUserTeeTime,
      isiOS,
      removeMyAccessory,
      removeMyPrice,
      removeMySquareItem,
      setNumHoles
    } = this.props;

    const {
      accessoryDisplayText,
      squareItemDisplayText,
      golfCourse,
      myAccessories,
      myPrices,
      mySquareItems,
      numHoles,
      priceDisplayText,
      tax,
      teeTime,
      totalPrice,
      totalPriceBeforeTax,
      userTeeTime
    } = this.props.checkout;

    const {
      isLoading,
      items
    } = this.props.square;

    return (
      <div>
        <div
          className="cursor text-primary"
          onClick={() => {
            configureCheckoutFromUserTeeTime();
            this.setState({
              isOpen: true
            });
          }}
        >
          <small className="c-underline">
            Edit &nbsp;&#x02192;
          </small>
        </div>
        {isOpen ?
          <div className="c-sidebar-checkout-container">
            <div className="c-sidebar-checkout-header">
              <div
                className="c-sidebar-checkout-close-btn"
                onClick={() => {
                  this.setState({
                    isOpen: false
                  });
                  closeSidebar();
                }}
              >
                &times;
              </div>
              Edit checkout
              <span className="m-l-1">
                <SquarePosIcon
                  height={17}
                  width={17}
                />
              </span>
            </div>
            <div className="c-sidebar-checkout-content">
              <Prices
                addMyPrice={addMyPrice}
                className="c-sidebar-checkout-dropdown"
                displayText={priceDisplayText}
                myPrices={myPrices}
                numHoles={numHoles}
                removeMyPrice={removeMyPrice}
                teeTime={teeTime}
              />
              <NumHoles
                className="c-sidebar-checkout-dropdown"
                myPrices={myPrices}
                numHoles={numHoles}
                setNumHoles={setNumHoles}
                teeTime={teeTime}
              />
              <Accessories
                accessories={golfCourse.golfCourseAccessories}
                addMyAccessory={addMyAccessory}
                className="c-sidebar-checkout-dropdown"
                displayText={accessoryDisplayText}
                myAccessories={myAccessories}
                numHoles={numHoles}
                removeMyAccessory={removeMyAccessory}
                teeTime={teeTime}
              />
              <SquareItems
                addMySquareItem={addMySquareItem}
                className="c-sidebar-checkout-dropdown"
                displayText={squareItemDisplayText}
                isLoading={isLoading}
                items={items}
                mySquareItems={mySquareItems}
                removeMySquareItem={removeMySquareItem}
              />
              <div className="c-sidebar-checkout-total-price">
                <div>&nbsp;</div>
                <TotalPrice
                  isAdmin
                  className="amount"
                  tax={tax}
                  totalPrice={totalPrice}
                  totalPriceBeforeTax={totalPriceBeforeTax}
                />
              </div>
              <div className="m-t-3 m-l-2">
                <BtnDefault
                  className="btn-block"
                  onClick={() => {
                    // if iOS open on native app
                    if(isiOS) {
                      const url = pointOfSale({
                        totalAmount: totalPrice,
                        state: {
                          isCustomFromSideBar: true,
                          userTeeTime: {
                            // need this id here to register user as payed
                            id: userTeeTime.id,
                            numPlayers: myPrices.length,
                            myAccessories,
                            myPrices,
                            tax,
                            totalPrice,
                            totalPriceBeforeTax
                          }
                        }
                      });
                      window.location.href = url;
                      return;
                    }
                    // otherwise open up in browser
                    window.open('https://squareup.com/terminal', '_blank');
                  }}
                >
                  Checkout with Square
                </BtnDefault>
              </div>
            </div>
          </div> :
          null
        }
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(SidebarPriceCustomization);
