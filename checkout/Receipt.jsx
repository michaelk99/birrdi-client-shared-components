import React from 'react';

import moment from 'moment';

import Modal from '@shared/modals/Modal';
import BtnDefault from '@shared/elements/BtnDefault';
import FormattedAccessory from '@shared/utils/FormattedAccessory';
import FormattedPrice from '@shared/utils/FormattedPrice';
import FormattedSquareItem from '@shared/utils/FormattedSquareItem';

import CustomFormContainer from '@shared/form/CustomFormContainer';
import CustomFormBody from '@shared/form/CustomFormBody';
import CustomFormHeader from '@shared/form/CustomFormHeader';

function Receipt(props) {
  const {
    hidePaymentCopy,
    isBooked,
    onClose,
    userTeeTime
  } = props;

  const {
    golfCourseName,
    priceExtras,
    startDate,
    startTime,
    userAccessories,
    userPrices
  } = userTeeTime;

  const totalPrice = userTeeTime.totalPrice;
  const headerTitle = isBooked ? 'Payment Receipt' : 'Reservation Receipt';

  return (
    <Modal
      initializeAsOpen
      onCloseClick={() => {
        onClose();
        window.location.href = '/app';
      }}
      positionAsAbsolute
    >
      <div className="m-t-3">
        <CustomFormContainer>
          <CustomFormHeader>
            <div className="text-center">
              <div>
                <h3>
                  {headerTitle}
                  <i className="fas fa-check-circle m-l-1"></i>
                </h3>
              </div>
              <div className="m-t-1">
                <div className="text-primary f1">
                  {moment(startTime, 'HH:mm:ss').format('h:mm a')}
                </div>
              </div>
              <div>
                {golfCourseName}
              </div>
              <div>
                {moment(startDate).format('MMM Do, YYYY')}
              </div>
            </div>
          </CustomFormHeader>
          <CustomFormBody>
            <div className="c-checkout-receipt">
              <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
                <div className="c-flex-child text-primary hidden-xs-down">
                  {isBooked ?
                    <span>Payment:</span> :
                    <span>Reservation:</span>
                  }
                </div>
                <div className="c-flex-child">
                  {isBooked ?
                    <div>{`$${totalPrice}`}</div> :
                    null
                  }
                  {hidePaymentCopy ?
                    <div>Confirmed</div> :
                    <div>Please pay ${totalPrice} at the golf course</div>
                  }
                </div>
              </div>
              <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
                <div className="c-flex-child text-primary hidden-xs-down">
                  <span>Players:</span>
                </div>
                <div className="c-flex-child">
                  <FormattedPrice
                    userPrices={userPrices}
                  />
                </div>
              </div>
              <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
                <div className="c-flex-child text-primary hidden-xs-down">
                  <span>Additional items:</span>
                </div>
                <div className="c-flex-child">
                  <FormattedAccessory
                    userAccessories={userAccessories}
                  />
                </div>
              </div>
              {priceExtras ?
                <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
                  <div className="c-flex-child text-primary hidden-xs-down">
                    <span>Square items:</span>
                  </div>
                  <div className="c-flex-child">
                    <FormattedSquareItem
                      priceExtras={priceExtras}
                    />
                  </div>
                </div> :
                null
              }
              <div className="d-flex c-center b-b-1 p-t-1 p-b-1">
                <div className="c-flex-child text-primary hidden-xs-down">
                  <span>Number of holes:</span>
                </div>
                <div className="c-flex-child">
                  <div>{`${userTeeTime.numHoles} holes`}</div>
                </div>
              </div>
              <div className="m-t-2">
                <BtnDefault
                  className="btn-block"
                  onClick={() => window.location.href = '/app'}
                >
                  Close
                </BtnDefault>
              </div>
            </div>
          </CustomFormBody>
        </CustomFormContainer>
      </div>
    </Modal>
  );
}

module.exports = Receipt;
