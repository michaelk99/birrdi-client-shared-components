import React from 'react';

import HttpTimer from './HttpTimer';

function Toastifications(props) {
  const {
    toastifications,
    removeToastification
  } = props;

  if(!toastifications.all.length) {
    return null;
  }

  return (
    <div>
      {toastifications.all.map(t => {
        const isError = t.message.includes('Error');
        return (
          <div key={`alert-container-${t.uuid}`}>
            <HttpTimer
              onFinish={() => removeToastification(t)}
            >
              <div className="c-alert-container">
                <div className="text-center">
                  <div className="c-alert-content">
                    {isError ?
                      <div style={{ background: '#d9534f' }}>
                        {t.message}
                      </div> :
                      <div>
                        {t.message}
                      </div>
                    }
                  </div>
                </div>
              </div>
            </HttpTimer>
          </div>
        );
      })}
    </div>
  );
}

module.exports = Toastifications;
