import React, { Component } from 'react';

class HttpTimer extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.onFinish();
    }, 7000);
  }
  componentWillUnmount() {
    this.props.onFinish();
  }
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

module.exports = HttpTimer;
