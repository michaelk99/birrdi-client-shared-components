import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside';
import isInteger from 'lodash/isInteger';

// HOC for handling simple data changes and for tracking editing
// and active states
function withEditing(WrappedComponent, handleClicksOutside = true) {

  // adds editing / active functionality
  // adds click outside listening functionality
  // adds easy ability to update and remove data
  class EnhancedComponent extends Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.updateData = this.updateData.bind(this);
      this.removeData = this.removeData.bind(this);
      this.toggleEditing = this.toggleEditing.bind(this);
      this.toggleActive = this.toggleActive.bind(this);
      this.setStateHOC = this.setStateHOC.bind(this);
      this.state = {
        isEditing: false,
        isActive: false,
        index: 0 // TODO name this better
      };
    }

    handleClickOutside() {
      if(handleClicksOutside) {
        this.setState({ isEditing: false, index: 0 });
      }
    }

    // backwards compatability
    handleChange(s) {
      this.setStateHOC(s);
    }

    setStateHOC(s) {
      this.setState({
        ...s
      });
    }

    updateData({ key, value, object }) {
      return this.props.updateData({ key, value, object });
    }

    removeData(data) {
      return this.props.removeData(data);
    }

    toggleEditing(index = 0) {
      if(!isInteger(index)) {
        index = 0;
      }
      return this.setState({
        isEditing: !this.state.isEditing,
        index
      });
    }

    toggleActive() {
      return this.setState({
        isActive: !this.state.isActive
      });
    }

    render() {
      return (
        <WrappedComponent
          {...this.state}
          {...this.props}
          handleChange={this.handleChange}
          removeData={this.removeData}
          setStateHOC={this.setStateHOC}
          toggleActive={this.toggleActive}
          toggleEditing={this.toggleEditing}
          updateData={this.updateData}
        />
      );
    }
  }
  return enhanceWithClickOutside(EnhancedComponent);
}

module.exports = withEditing;
