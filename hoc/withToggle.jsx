import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside';

// HOC that gives toggle functionality to a component on click
function withToggle(WrappedComponent) {

  // adds toggle functionality to a component
  // adds click outside listening functionality
  // adds easy ability to update and remove data
  class EnhancedComponent extends Component {
    constructor(props) {
      super(props);
      this.handleClickOutside = this.handleClickOutside.bind(this);
      this.state = {
        isOpen: false
      };
    }

    handleClickOutside() {
      this.setState({
        isOpen: false
      });
    }

    render() {
      const {
        isOpen
      } = this.state;

      return (
        <WrappedComponent
          {...this.state}
          {...this.props}
          onClick={() => {
            this.setState({
              isOpen: !isOpen
            });
          }}
        />
      );
    }
  }

  return enhanceWithClickOutside(EnhancedComponent);
}

module.exports = withToggle;
