import React from 'react';

import alertify from '@utils/alertify';

class Confirmations extends React.Component {
  constructor(props) {
    super(props);
    this.showConfirmations = this.showConfirmations.bind(this);

    // keep track of displayed confirmations in local state to prevent dup
    // confirmations
    this.state = {
      displayed: {},
      confirmations: this.props.confirmations
    };
  }
  componentDidMount() {
    const {
      confirmations,
      closeConfirmation
    } = this.props;

    const {
      all
    } = confirmations;

    if (!all.length) {
      return;
    }

    this.showConfirmations({
      confirmations,
      closeConfirmation
    });
  }
  componentWillReceiveProps(nextProps) {
    const {
      confirmations,
      closeConfirmation
    } = nextProps;

    const {
      all
    } = confirmations;

    if (!all.length) {
      return;
    }

    // if there are displayed confirmations, make sure not to render
    // a dup confirmation
    if(Object.keys(this.state.displayed).length) {
      // if the length is the same then there's a chance we might render
      // a dup confirmation

      if(confirmations.all.length === this.state.confirmations.all.length) {
        // if all confirmations are the same then do not pass down to the
        // next render cycle
        const compared = confirmations.all.filter(c => {
          return this.state.confirmations.byId[c.id];
        });

        if(compared.length === confirmations.all.length) {
          return;
        }
      }
    }

    this.showConfirmations({
      confirmations,
      closeConfirmation
    });
  }
  showConfirmations({ confirmations, closeConfirmation }) {
    const newState = {};

    confirmations.all.forEach(confirmation => {
      if(this.state.displayed[confirmation.id]) {
        return;
      }

      newState[confirmation.id] = confirmation;

      alertify.confirm({
        message: confirmation.message,
        onSuccess: () => {
          confirmation.onSuccess();
          closeConfirmation(confirmation);

          delete this.state.displayed[confirmation.id];
          this.setState({
            displayed: { ...this.state.displayed }
          });
        },
        onCancel: () => {
          closeConfirmation(confirmation);

          delete this.state.displayed[confirmation.id];
          this.setState({
            displayed: { ...this.state.displayed }
          });
        },
        okBtn: 'Yes',
        cancelBtn: 'No, go back'
      });

    });

    this.setState({
      displayed: newState
    });
  }
  render() {
    return null;
  }
}

module.exports = Confirmations;
