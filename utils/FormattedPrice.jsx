import React from 'react';

import Blocks from './Blocks';

import {
  createPriceDisplayTextList
} from '@utils/displayText';

function FormattedPrice(props) {
  const {
    userPrices
  } = props;

  const list = createPriceDisplayTextList(userPrices);

  if(!list.length) {
    // the list could be empty for members, since as of now members only
    // reserve by # of players (1 player, 2 players, etc.)
    return null;
  }

  return (
    <Blocks list={list} />
  );
}

module.exports = FormattedPrice;
