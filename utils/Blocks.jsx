import React from 'react';

function Blocks(props) {
  const {
    list
  } = props;

  const blocks = list.map(item => {
    const {
      count,
      name
    } = item;

    return (
      <div
        className="c-tag c-tag-magenta c-tag-block"
        key={`block-item-${name}-${count}`}
      >
        {`${count} ${name}`}
      </div>
    );
  });

  return (
    <div>
      {blocks}
    </div>
  );
}

module.exports = Blocks;
