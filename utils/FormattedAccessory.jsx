import React from 'react';

import Blocks from './Blocks';

import {
  createAccessoryDisplayTextList
} from '@utils/displayText';

function FormattedAccessory(props) {
  const {
    userAccessories
  } = props;

  const list = createAccessoryDisplayTextList(userAccessories);

  if(!list.length)  {
    return (
      <div className="block">
        No additional items
      </div>
    );
  }

  return (
    <Blocks list={list} />
  );
}

module.exports = FormattedAccessory;
