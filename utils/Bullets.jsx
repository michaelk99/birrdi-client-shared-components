import React from 'react';

function Bullets(props) {
  const {
    list
  } = props;

  const bullets = list.map(item => {
    const {
      count,
      name
    } = item;

    return (
      <pre
        className="text-left text-secondary"
        key={`bullet-item-${name}-${count}`}
      >
        &#x02022; {`${count} ${name}`}
      </pre>
    );
  });

  return (
    <div>{bullets}</div>
  );
}

module.exports = Bullets;
