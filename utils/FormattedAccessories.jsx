import React from 'react';

import FormattedAccessory from './FormattedAccessory';

function FormattedAccessories(props) {
  const {
    userTeeTimes
  } = props;

  const formattedAccessories = userTeeTimes.map(utt => {
    return <FormattedAccessory userAccessories={utt.userAccessories} />;
  });

  return {formattedAccessories};
}

module.exports = FormattedAccessories;
