import React from 'react';

import Blocks from './Blocks';

import {
  createSquareItemDisplayTextList
} from '@utils/displayText';

function FormattedSquareItem(props) {
  const {
    priceExtras
  } = props;

  if(!priceExtras) {
    return (
      <div className="block">
        No additional items
      </div>
    );
  }

  const priceExtrasJSON = JSON.parse(priceExtras);

  if(!priceExtrasJSON.squareItems) {
    return (
      <div className="block">
        No additional items
      </div>
    );
  }

  const {
    squareItems
  } = priceExtrasJSON;

  const list = createSquareItemDisplayTextList(squareItems);

  if(!list.length)  {
    return (
      <div className="block">
        No square items
      </div>
    );
  }

  return (
    <Blocks list={list} />
  );
}

module.exports = FormattedSquareItem;
