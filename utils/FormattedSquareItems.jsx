import React from 'react';

import FormattedSquareItem from './FormattedSquareItem';

function FormattedSquareItems(props) {
  const {
    userTeeTimes
  } = props;

  const formattedSquareItems = userTeeTimes.map(utt => {
    return (
      <FormattedSquareItem
        priceExtras={utt.priceEtxras}
      />
    );
  });

  return formattedSquareItems;
}

module.exports = FormattedSquareItems;
