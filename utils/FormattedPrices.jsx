import React from 'react';

import FormattedPrice from './FormattedPrice';

function FormattedPrices(props) {
  const {
    userTeeTimes
  } = props;

  const formattedPrices = userTeeTimes.map(utt => {
    return <FormattedPrice userPrices={utt.userPrices} />;
  });

  return {formattedPrices};
}

module.exports = FormattedPrices;
