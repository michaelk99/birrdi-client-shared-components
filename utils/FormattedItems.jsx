import React from 'react';

import concat from 'lodash/concat';
import flatten from 'lodash/flatten';

import {
  createPriceDisplayTextList,
  createAccessoryDisplayTextList,
  createSquareItemDisplayTextList
} from '@utils/displayText';

function FormattedItems(props) {
  const {
    userPrices,
    userAccessories,
    priceExtras
  } = props;

  const userPricesDT = createPriceDisplayTextList(userPrices);
  const userAccessoriesDT = createAccessoryDisplayTextList(userAccessories);
  let squareItemsDT = [];

  if(priceExtras && priceExtras.length) {
    try {
      const json = JSON.parse(priceExtras);
      squareItemsDT = createSquareItemDisplayTextList(json.squareItems);
    } catch(e) {
      squareItemsDT = createSquareItemDisplayTextList(priceExtras);
    }
  }

  const all = flatten(concat(userPricesDT, userAccessoriesDT, squareItemsDT));
  const items = all.map(item => {
    const {
      count,
      name
    } = item;

    return (
      <div
        className="c-tag c-tag-magenta"
        key={`block-item-${name}-${count}`}
      >
        <span>{`${count} ${name}`}</span>
      </div>
    );
  });

  return (
    <React.Fragment>
      {items}
    </React.Fragment>
  );
}

module.exports = FormattedItems;
