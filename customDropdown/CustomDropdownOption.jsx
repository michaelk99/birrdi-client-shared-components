import React from 'react';

import CustomNumberPicker from './CustomNumberPicker';

function CustomDropdownOption(props) {
  const {
    amount,
    name,
    numSelected,
    onAdd,
    onRemove,
    option
  } = props;

  return (
    <div
      className="c-dropdown-option"
      onClick={e => {
        // default behavior is to stop propagation
        e.preventDefault();
        e.stopPropagation();
      }}
    >
      <div className="c-dropdown-option-cell">
        <div className="c-cell-name">{name}</div>
        <div>
          <small>${amount}</small>
        </div>
      </div>
      <div className="c-dropdown-option-cell">
        <CustomNumberPicker
          numSelected={numSelected}
          onAdd={onAdd}
          onRemove={onRemove}
          option={option}
        />
      </div>
    </div>
  );
}

module.exports = CustomDropdownOption;
