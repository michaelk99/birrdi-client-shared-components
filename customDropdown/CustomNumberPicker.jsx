import React from 'react';

function CustomNumberPicker(props) {
  const {
    numSelected,
    onAdd,
    onRemove,
    option
  } = props;

  return (
    <div className="c-number-picker">
      <div
        className="picker plus"
        onClick={() => onAdd(option)}
      >
        <i className="fa fa-plus"></i>
      </div>
      <div className="text-middle">
        {numSelected}
      </div>
      <div
        className="picker minus"
        onClick={() => onRemove(option)}
      >
        <i className="fa fa-minus"></i>
      </div>
    </div>
  );
}

module.exports = CustomNumberPicker;
