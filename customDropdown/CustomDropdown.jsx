import React, { Component } from 'react';

import enhanceWithClickOutside from 'react-click-outside';
import classNames from 'classnames';

import isArray from 'lodash/isArray';

class CustomDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      // keep this in state just so we can rerender later
      displayText: this.props.displayText
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.toggle = this.toggle.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      displayText
    } = nextProps;

    // always rerender
    this.setState({
      displayText
    });
  }
  handleClickOutside() {
    this.setState({
      isOpen: false
    });
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const {
      isOpen,
      displayText
    } = this.state;

    const {
      children
    } = this.props;

    const className = classNames({
      'open': isOpen,
      'closed': !isOpen
    });

    if(!isArray(displayText)) {
      return (
        <div
          className="c-dropdown"
          onClick={() => this.setState({ isOpen: !isOpen })}
          style={{ width: '100%' }}
        >
          <div className="c-dropdown-toggle form-control">
            <span className="text-placeholder">
              {displayText}
            </span>
          </div>
          <div className={`c-dropdown-menu ${className}`}>
            <div className="c-dropdown-options">
              {children}
            </div>
          </div>
          <div className="c-dropdown-caret">
            &#x025BE;
          </div>
        </div>
      );
    }

    return (
      <div
        className="c-dropdown"
        onClick={() => this.setState({ isOpen: !isOpen })}
        style={{ width: '100%' }}
      >
        <div className="c-dropdown-toggle form-control">
          <div className="d-table">
            <div className="d-table-cell">
              {displayText.map((dt, i) => {
                return (
                  <div
                    className="c-tag c-tag-magenta"
                    key={`${dt}-${i}`}
                  >
                    <span>{dt}</span>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="c-dropdown-caret">
          &#x025BE;
        </div>
        <div className={`c-dropdown-menu ${className}`}>
          <div className="c-dropdown-options">
            {children}
          </div>
        </div>
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(CustomDropdown);
