import React from 'react';
import classNames from 'classnames';

function Loading({ isLoading }) {
  const className = classNames('c-loading-container', {
    'c-loading-container-hide': !isLoading
  });
  return (
    <div className={className}>
      <div className="c-loading">
        <div className="loader-icon"></div>
      </div>
    </div>
  );
}

module.exports = Loading;
