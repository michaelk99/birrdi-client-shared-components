import React from 'react';

const year = new Date().getFullYear();

function GolfCourseFooter() {
  return (
    <div className="c-footer">
      <div>© {year} Birrdi, Inc.</div>
      <div>Made with &#x2665; in NYC</div>
      <div>
        <a
          href="mailto:birrdihello@gmail.com"
          target="_blank"
        >
          Contact support&nbsp;&nbsp;
          <i className="far fa-envelope"></i>
        </a>
      </div>
    </div>
  );
}

module.exports = GolfCourseFooter;
