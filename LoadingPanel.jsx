import React from 'react';
import classNames from 'classnames';

function LoadingPanel({ isLoading }) {
  const className = classNames('c-loading-panel', {
    'c-loading-panel-hide': !isLoading
  });

  return (
    <div className={className}>
      <div className="c-loading">
        <div className="loader-icon"></div>
      </div>
    </div>
  );
}

module.exports = LoadingPanel;
