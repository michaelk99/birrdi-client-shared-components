import React from 'react';

function LoadingGif(props) {
  return (
    <svg
      height="60"
      style={{
        animation: 'spin 650ms infinite linear'
      }}
      viewBox="0 0 75 75"
      width="200"
      {...props}
    >
      <linearGradient id="spinner_dark_1" gradientUnits="userSpaceOnUse" x1="0" y1="56.25" x2="75" y2="56.25">
        <stop offset="0" style={{'stopColor': '#000000', 'stopOpacity': '1'}} />
        <stop offset="1" style={{'stopColor': '#000000', 'stopOpacity': '0'}} />
      </linearGradient>
      <path fill="url(#spinner_dark_1)" d="M37.5,75C16.8,75,0,58.2,0,37.5h3C3,56.5,18.5,72,37.5,72S72,56.5,72,37.5h3 C75,58.2,58.2,75,37.5,75z" />
      <linearGradient id="spinner_dark_2" gradientUnits="userSpaceOnUse" x1="0" y1="19.5" x2="75" y2="19.5">
        <stop offset="0" style={{'stopColor': '#000000', 'stopOpacity': '1'}} />
        <stop offset="1" style={{'stopColor': '#000000', 'stopOpacity': '0'}} />
      </linearGradient>
      <path fill="url(#spinner_dark_2)" d="M37.5,0C16.8,0,0,16.8,0,37.5h3C3,18.5,18.5,3,37.5,3S72,18.5,72,37.5c0,0.8,0.7,1.5,1.5,1.5 s1.5-0.7,1.5-1.5C75,16.8,58.2,0,37.5,0z" />
    </svg>
  );
}

module.exports = LoadingGif;
