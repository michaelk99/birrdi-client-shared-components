'';

import React from 'react';
import Container from '@shared/Container';

function NotFoundRoute({ message }) {
  return (
    <Container>
      <div className="route-not-found">
        <div className="not-found-message">
          <h3>
            {message || 'Oops. Looks like this page doesn\'t exist'}
          </h3>
        </div>
      </div>
    </Container>
  );
}

module.exports = NotFoundRoute;
