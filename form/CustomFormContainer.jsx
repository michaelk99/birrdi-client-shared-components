'';

import React from 'react';

function CustomFormContainer({ children }) {
  return (
    <div className="c-form-container">
      {children}
    </div>
  );
}

module.exports = CustomFormContainer;
