import React from 'react';

function CustomFormHeader({ children }) {
  return (
    <div className="c-form-header">
      {children}
    </div>
  );
}

module.exports = CustomFormHeader;
