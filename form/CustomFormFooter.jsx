'';

import React from 'react';

function CustomFormFooter({ children }) {
  return (
    <div className="c-form-footer" style={{opacity: 1}}>
      {children}
    </div>
  );
}

module.exports = CustomFormFooter;
