'';

import React from 'react';

function CustomFormBody({ children }) {
  return (
    <div className="c-form-body">
      {children}
    </div>
  );
}

module.exports = CustomFormBody;
