import React from 'react';

function Menu(props) {

  const {
    children
  } = props;

  return (
    <div className="menu-snapshot">
      <div className="menu-snapshot-content">
        {children}
      </div>
    </div>
  );
}

module.exports = Menu;
