import React from 'react';

import classNames from 'classnames';

function Sidebar(props) {
  const {
    children,
    className,
    left,
    right,
    onClose
  } = props;

  const sidebarClassName = classNames({
    [className]: !!className,
    'c-sidebar-container-left': left,
    'c-sidebar-container-right': right || (!left && !right)
  });

  return (
    <div className={sidebarClassName}>
      <div
        className="c-sidebar-close-btn"
        onClick={onClose}
      >
        &times;
      </div>
      {children}
    </div>
  );
}

module.exports = Sidebar;
