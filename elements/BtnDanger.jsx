'';
import React from 'react';

function BtnDanger({ onClick, disabled, className = '', children }) {
  const klassName = (
    disabled ?
    `btn btn-danger-outline disabled ${className}` :
    `btn btn-danger-outline ${className}`
  );
  return (
    <div className={klassName} onClick={onClick}>
      {children}
    </div>
  );
}

module.exports = BtnDanger;
