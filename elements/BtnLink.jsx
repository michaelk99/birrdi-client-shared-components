'';
import React from 'react';

function BtnLink({ onClick, disabled, className = '', children}) {
  const klassName = (
    disabled ?
    `btn-link cursor disabled ${className}` :
    `btn-link cursor ${className}`
  );
  return (
    <div
      className={klassName}
      onClick={onClick}
    >
      {children}
    </div>
  );
}

module.exports = BtnLink;
