'';
import React from 'react';

function BtnSuccess({ onClick, disabled, className = '', children }) {
  const klassName = (
    disabled ?
    `btn btn-success-outline disabled ${className}` :
    `btn btn-success-outline ${className}`
  );

  return (
    <div className={klassName} onClick={onClick}>
      {children}
    </div>
  );
}

module.exports = BtnSuccess;
