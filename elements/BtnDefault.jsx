import React from 'react';

import classNames from 'classnames';

import LoadingGif from '@shared/icons/LoadingGif';

function BtnDefault(props) {
  const {
    onClick,
    disabled,
    className = '',
    children,
    isLoading
  } = props;

  const btnClassName = classNames(className, {
    'btn btn-default-outline disabled' : disabled,
    'btn btn-default-outline': !disabled
  });

  if(isLoading) {
    return (
      <div className="text-center">
        <LoadingGif />
      </div>
    );
  }

  return (
    <div
      className={btnClassName}
      onClick={onClick}
    >
      {children}
    </div>
  );
}

module.exports = BtnDefault;
