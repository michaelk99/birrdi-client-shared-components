import React from 'react';

function BtnPrimary({ onClick, disabled, className = '', children }) {
  const klassName = (
    disabled ?
    `btn btn-default disabled ${className}` :
    `btn btn-default ${className}`
  );
  return (
    <div className={klassName} onClick={onClick}>
      {children}
    </div>
  );
}

module.exports = BtnPrimary;
