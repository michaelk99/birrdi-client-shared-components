'';
import React from 'react';

function Select({
  className,
  value = '-1',
  onChange,
  children
}) {
  return (
    <select
      className={className ? className : 'c-select form-control'}
      onChange={onChange}
      value={value}
    >
      {children}
    </select>
  );
}

module.exports = Select;
