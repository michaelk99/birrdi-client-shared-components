'';
import React from 'react';

function Checkbox({ className, onChange, checked, children }) {
  return (
    <label className={className ? className : 'c-input c-checkbox'}>
      <input
        checked={checked || false}
        onChange={onChange}
        type="checkbox"
      >
      </input>
      <span className="c-indicator"></span>
      {children}
    </label>
  );
}

module.exports = Checkbox;
