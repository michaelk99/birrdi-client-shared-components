'';
import React from 'react';

function TextArea({
  className,
  value = '',
  placeholder,
  onChange,
  readonly,
  resize = 'inherit'
}) {
  return (
    <textarea
      className={className ? className : 'form-control'}
      onChange={onChange}
      placeholder={placeholder}
      readOnly={readonly}
      resize={resize}
      style={{ resize }}
      value={value}
    />
  );
}

module.exports = TextArea;
