'';
import React from 'react';

function Input({
  className,
  value,
  placeholder,
  onChange,
  readonly = false,
  type = 'text',
  style = {}
}) {
  return (
    <input
      className={className ? className : ''}
      onChange={onChange}
      placeholder={placeholder}
      readOnly={readonly}
      style={style}
      type={type}
      value={value}
    />
  );
}

module.exports = Input;
