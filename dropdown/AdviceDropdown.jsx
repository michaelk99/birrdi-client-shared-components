import React, { Component } from 'react';

import enhanceWithClickOutside from 'react-click-outside';
import classNames from 'classnames';

import BtnDefault from '@shared/elements/BtnDefault';

class AdviceDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      value: ''
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }
  handleClickOutside() {
    this.setState({
      isOpen: false
    });
  }
  elementClick(onClickFn) {
    return onClickFn();
  }
  render() {
    const { isOpen, value } = this.state;

    const dropdownClassName = classNames('dropdown hidden-sm-down', {
      'open': isOpen,
      'closed': !isOpen
    });

    return (
      <div
        className={dropdownClassName}
      >
        <div className="dropdown-toggle-no-icon">
          <div
            className="menu-nav-cta-special"
            onClick={() => this.setState({ isOpen: !isOpen })}
          >
            Get Advice On Your Swing&nbsp;&nbsp;
            <i className="fa fa-caret-down"></i>
          </div>
        </div>
        <div
          className="dropdown-menu"
        >
          <div className="p-a-1">
            <small className="text-secondary">
              Explain what you would like help with and we will get back
              to you within 24 hours.
            </small>
            <div className="m-t-1">
              <textarea
                className="f6 form-control"
                onChange={e => this.setState({
                  value: e.target.value
                })}
                placeholder="Start writing here..."
                rows={4}
                value={value}
              >
              </textarea>
            </div>
            <BtnDefault
              className="m-t-2 btn-block"
              onClick={() => {
                this.props.submitAdvice({
                  message: value
                });
                this.setState({
                  isOpen: false,
                  value: ''
                });
              }}
            >
              Submit
            </BtnDefault>
          </div>
        </div>
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(AdviceDropdown);
