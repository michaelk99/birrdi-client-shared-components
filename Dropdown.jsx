import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside';
import classNames from 'classnames';

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }
  handleClickOutside() {
    this.setState({
      isOpen: false
    });
  }
  elementClick(onClickFn) {
    return onClickFn();
  }
  render() {
    const {
      isOpen
    } = this.state;

    const {
      noIcon
    } = this.props;

    const dropdownClassName = classNames('dropdown', {
      'open': isOpen,
      'closed': !isOpen
    });

    const iconClassName = classNames({
      'dropdown-toggle-no-icon': noIcon,
      'dropdown-toggle': !noIcon
    });

    const dropdownMenuClassName = classNames('dropdown-menu', {
      [this.props.className]: this.props.className
    });

    return (
      <div
        className={dropdownClassName}
        onClick={() => this.setState({ isOpen: !isOpen })}
      >
        <div className={iconClassName}>
          {this.props.customToggle ?
            this.props.customToggle :
            <div className="d-inline">
              <div>{this.props.toggleText}</div>
            </div>
          }
        </div>
        <div
          className={dropdownMenuClassName}
          style={this.props.style ? this.props.style : {}}
        >
          <ul>
            {this.props.children}
          </ul>
        </div>
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(Dropdown);
