import React from 'react';
import omit from 'lodash/omit';

export default class ReactGoogleAutocomplete extends React.Component {

  constructor(props) {
    super(props);
    this.autocomplete = null;
    this.event = null;
    this.onSelected = this.onSelected.bind(this);

    this.state = {
      value: this.props.location || ''
    };
  }

  componentDidMount() {
    // TODO show a toastification that something is wrong if there
    // is no `window.google` object
    if(!window.google) {
      return null;
    }

    // if config is passed in as a prop, make sure to pass it
    // in to the google auto complete func
    if(this.props.config) {
      this.autocomplete = new window.google.maps.places.Autocomplete(this.input, this.props.config);
    } else {
      this.autocomplete = new window.google.maps.places.Autocomplete(this.input);
    }
    this.event = this.autocomplete.addListener('place_changed', this.onSelected);
  }

  componentWillReceiveProps(nextProps) {
    if(!window.google) {
      return null;
    }

    if(this.props.location !== nextProps.location) {
      this.setState({
        value: nextProps.location
      });
    }
  }

  componentWillUnmount() {
    if(!window.google) {
      return null;
    }

    if(this.event) {
      this.event.remove();
    }
  }

  onSelected() {
    if (this.props.onSelected) {
      this.props.onSelected(this.autocomplete.getPlace());
    }
  }

  render() {
    const {
      value
    } = this.state;

    const props = omit(this.props, ['onSelected']);

    return (
      <div>
        <input
          {...props}
          onChange={e => this.setState({ value: e.target.value })}
          ref={input => this.input = input}
          value={value}
        />
      </div>
    );
  }
}
