import React from 'react';

import Dropdown from '@shared/Dropdown';

function ClubPickerNav(props) {
  const {
    activeClub,
    clubs,
    setActiveClub,
    setToastification
  } = props;

  if(!clubs.length) {
    return null;
  }

  if(!activeClub) {
    return null;
  }

  const options = clubs.map(c => {
    return (
      <li
        className="text-primary"
        key={`club-picker-${c.id}`}
        onClick={() => {
          setActiveClub(c);
          setToastification({
            uuid: `club-id-${c.id}`,
            message: `Switched to ${c.name}`
          });
        }}
      >
        <div>{c.name}</div>
      </li>
    );
  });

  return (
    <div className="text-center m-t-1">
      <div className="default-input d-inline-block">
        <Dropdown customToggle={`${activeClub.name}`}>
          {options}
        </Dropdown>
      </div>
    </div>
  );
}

module.exports = ClubPickerNav;
