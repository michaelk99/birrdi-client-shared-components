import React from 'react';
import { Link } from 'react-router-dom';

import Dropdown from '@shared/Dropdown';

function ClubPicker(props) {
  const {
    activeClub,
    clubs,
    setActiveClub
  } = props;

  if(!clubs.length) {
    return (
      <div className="p-a-3 text-center">
        <Link to="/setup?t=clubs">
          <span className="text-primary dropdown-border">
            Create a club&nbsp;&nbsp;
            <i className="fas fa-long-arrow-alt-right"></i>
          </span>
        </Link>
      </div>
    );
  }

  const options = clubs.map(c => {
    return (
      <li
        className="text-primary"
        key={`club-picker-${c.id}`}
        onClick={() => setActiveClub(c)}
      >
        <div>{c.name}</div>
      </li>
    );
  });

  if(!activeClub) {
    return (
      <div className="text-center text-primary p-a-3">
        <div className="d-inline-block dropdown-border">
          <Dropdown customToggle="Select a club">
            {options}
          </Dropdown>
        </div>
      </div>
    );
  }

  return (
    <div className="text-center">
      <div className="d-inline-block">
        <Dropdown customToggle={`${activeClub.name}`}>
          {options}
        </Dropdown>
      </div>
    </div>
  );
}

module.exports = ClubPicker;
