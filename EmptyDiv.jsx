import React from 'react';

function EmptyDiv({ body, children }) {
  if(body) {
    return (
      <div className="p-t-2 p-b-1 text-center">
        <h4>{body}</h4>
      </div>
    );
  }
  return (
    <div className="p-t-2 p-b-1 text-center">
      {children}
    </div>
  );
}

module.exports = EmptyDiv;
