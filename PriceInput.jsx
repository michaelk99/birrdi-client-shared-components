import React from 'react';

import enhanceWithClickOutside from 'react-click-outside';

class PriceInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.state = {
      isEditing: false
    };
  }
  handleClickOutside() {
    this.setState({
      isEditing: false
    });
  }
  render() {
    const {
      className = '',
      priceAmount,
      sliderCallback
    } = this.props;

    const {
      isEditing
    } = this.state;

    const priceValue = priceAmount != null ? priceAmount : '';
    const displayPrice = priceAmount != null ? `$${priceAmount}` : '$';

    if(isEditing) {
      return (
        <input
          autoFocus
          onChange={e => {
            return sliderCallback(e.target.value);
          }}
          placeholder="10"
          style={{
            width: '65px'
          }}
          type="number"
          value={priceValue}
        />
      );
    }

    return (
      <div
        className="default-input"
        onClick={() => this.setState({
          isEditing: true
        })}
      >
        <span className={className}>
          {displayPrice}
        </span>
      </div>
    );
  }
}

module.exports = enhanceWithClickOutside(PriceInput);
