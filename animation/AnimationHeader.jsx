'';

import React from 'react';

function AnimationHeader({ children }) {
  return (
    <div className="c-animation-header">
      {children}
    </div>
  );
}

module.exports = AnimationHeader;
