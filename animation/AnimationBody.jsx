'';

import React from 'react';

function AnimationBody({ children }) {
  return (
    <div className="c-animation-body">
      {children}
    </div>
  );
}

module.exports = AnimationBody;
