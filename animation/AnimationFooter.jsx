'';

import React from 'react';

function AnimationFooter({ children }) {
  return (
    <div className="c-animation-footer">
      {children}
    </div>
  );
}

module.exports = AnimationFooter;
