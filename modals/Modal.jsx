import React from 'react';
import ReactDOM from 'react-dom';

import classNames from 'classnames';
import styles from './Modal.scss';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.modalListener = this.modalListener.bind(this);
  }
  modalListener(e) {
    this.props.onClose(e);
  }
  componentDidMount() {
    document.querySelector('.modal-background').addEventListener('click', e => {
      this.modalListener(e);
    });
  }
  componentWillUnmount() {
    document.querySelector('.modal-background').removeEventListener('click', this.modalListener);
  }
  render() {
    const {
      children,
      className,
      isOpen = true,
      onClose,
      isCustomContent
    } = this.props;

    const klassName = classNames('modal', {
      'is-active': isOpen
    });

    const contentClassName = classNames({
      // allow ability to not include `modal-content` css
      'modal-content': !isCustomContent,
      // allow ability to set custom class on modal-content
      [className]: !!className,
      // always true for now
      [styles.ModalContentFullHeight]: true
    });

    const root = document.querySelector('#root');

    // React does *not* create a new div. It renders the children into
    // `domNode`.  `domNode` is any valid DOM node, regardless of its location
    // in the DOM.
    return ReactDOM.createPortal(
      (<div className={klassName}>
        <div className="modal-background"></div>
        <div className={contentClassName}>
          {children}
        </div>
        <button
          aria-label="close"
          className="modal-close is-large"
          onClick={onClose}
        >
        </button>
      </div>),
      root
    );
  }
}

export default Modal;
