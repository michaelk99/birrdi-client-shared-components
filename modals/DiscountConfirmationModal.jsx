import React from 'react';

import Modal from '@shared/modals/Modal';

import CustomFormContainer from '@shared/form/CustomFormContainer';
import CustomFormBody from '@shared/form/CustomFormBody';
import CustomFormHeader from '@shared/form/CustomFormHeader';

import {
  DISCOUNT_AMOUNT
} from 'constants';

function DiscountConfirmationModal(props) {

  const {
    discount,
    resetDiscount
  } = props;

  const {
    amount,
    confirmationMessage,
    isFromRefund,
    type,
    userTeeTime
  } = discount;

  const isAmount = type === DISCOUNT_AMOUNT;

  let headerTitle = 'Discount confirmation';
  if(userTeeTime) {
    headerTitle = 'Direct discount confirmation';
  }
  if(isFromRefund) {
    headerTitle = 'Refund confirmation';
  }

  return (
    <Modal
      displayBodySmall
      includeCloseBtn
      initializeAsOpen
      onCloseClick={() => {
        resetDiscount();
        // TODO - use browser history
        // reset
        window.location.href = '/';
      }}
      positionAsAbsolute
    >
      <div className="m-t-3">
        <CustomFormContainer>
          <CustomFormHeader>
            <div className="text-center">
              <h3>
                  {headerTitle}
                  <i className="fas fa-check-circle m-l-1 text-success"></i>
              </h3>
            </div>
          </CustomFormHeader>
          <CustomFormBody>
            <div className="d-flex c-center">
              <div>{confirmationMessage}</div>
              <div className="m-t-1">
                {isAmount &&
                  <span>Amount: {`$${amount} off`}</span>
                }
                {!isAmount &&
                  <span>Percent: {`${amount}% off`}</span>
                }
              </div>
            </div>
          </CustomFormBody>
        </CustomFormContainer>
      </div>
    </Modal>
  );
}

module.exports = DiscountConfirmationModal;
