import React from 'react';
import PropTypes from 'prop-types';

import moment from 'moment';

import Container from '@containers/Container';
import Modal from '@shared/modals/Modal';
import BtnDefault from '@shared/elements/BtnDefault';

import CustomFormContainer from '@shared/form/CustomFormContainer';
import CustomFormBody from '@shared/form/CustomFormBody';
import CustomFormHeader from '@shared/form/CustomFormHeader';

import {
  DISCOUNT_AMOUNT,
  DISCOUNT_PERCENT
} from 'constants';

class DiscountModal extends React.Component {
  render() {
    const {
      showConfirmation,
      resetDiscount,
      sendDiscount,
      setDiscountAmount,
      setDiscountType,
      onClose,
      activeGolfCourse,
      discount
    } = this.props;

    const {
      amount,
      isFromRefund,
      message,
      teeTime,
      type,
      userTeeTime
    } = discount;

    const isAmount = type === DISCOUNT_AMOUNT;
    const amountMessage = isAmount ? 'Enter amount' : 'Enter percentage';

    return (
      <Container
        {...this.props}
        shouldAllowSingle={0}
      >
        <Modal
          displayBodySmall
          includeCloseBtn
          initializeAsOpen
          onCloseClick={() => {
            resetDiscount();
            if(onClose) {
              return onClose();
            }
            // otherwise fallback to this
            this.context.router.push('/overview?reset=true');
          }}
          positionAsAbsolute
        >
          <div className="m-t-3">
            <CustomFormContainer>
              <CustomFormHeader>
                {!userTeeTime &&
                  <div className="text-center">
                    <h2>
                      {activeGolfCourse.name}
                    </h2>
                    <div style={{
                      marginTop: '.5rem'
                    }}
                    >
                      <h3>
                        {moment(teeTime.startDate).format('h:mm a')}
                      </h3>
                    </div>
                    <div>
                      <small>
                        {moment(teeTime.startDate).format('MMM Do, YYYY')}
                      </small>
                    </div>
                    <div className="m-t-1">
                      Note: there are {teeTime.numSpotsLeft} spots left
                    </div>
                  </div>
                }
                {userTeeTime &&
                  <div className="text-center">
                    <h3>
                      {isFromRefund ?
                        'Refund golfer' :
                        'Send a direct discount'
                      }
                    </h3>
                  </div>
                }
              </CustomFormHeader>
              <CustomFormBody>
                <div className="d-flex c-center c-checkout-tabs">
                  <div
                    className={`${isAmount ? 'tab active' : 'tab'}`}
                    onClick={() => setDiscountType(DISCOUNT_AMOUNT)}
                  >
                    <span>Amount</span>
                  </div>
                  <div
                    className={`${isAmount ? 'tab' : 'tab active'}`}
                    onClick={() => setDiscountType(DISCOUNT_PERCENT)}
                  >
                    <span>Percent</span>
                  </div>
                </div>
                <div className="m-t-3">{message}</div>
                <div className="m-t-3 d-flex c-center">
                  {isAmount &&
                    <i className="fa fa-usd text-primary m-r-1"></i>
                  }
                  <input
                    className="not-form-control"
                    onChange={e => setDiscountAmount(e.target.value)}
                    placeholder={amountMessage}
                    value={amount || ''}
                  />
                  {!isAmount &&
                    <i className="m-l-1 fa fa-percent text-primary"></i>
                  }
                  <BtnDefault
                    className="m-l-2"
                    onClick={() => {
                      showConfirmation({
                        message: 'Please confirm this action. Continue?',
                        onSuccess: () => {
                          sendDiscount({
                            amount,
                            type,
                            teeTime,
                            userTeeTime
                          });
                        }
                      });
                    }}
                  >
                    {isFromRefund ? 'Send Refund' : 'Send Discount'}
                    <i className="m-l-1 fa fa-paper-plane-o"></i>
                  </BtnDefault>
                </div>
              </CustomFormBody>
            </CustomFormContainer>
          </div>
        </Modal>
      </Container>
    );
  }
}

DiscountModal.contextTypes = {
  router: PropTypes.object.isRequired
};

module.exports = DiscountModal;
