import React from 'react';
import classNames from 'classnames';

class Toggle extends React.Component {
  constructor(props) {
    super(props);
    const {
      isChecked
    } = props;

    this.state = {
      isChecked: isChecked || false
    };
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.isChecked !== this.state.isChecked) {
      this.setState({
        isChecked: nextProps.isChecked
      });
    }
  }
  render() {
    const {
      isChecked
    } = this.state;

    const {
      onClick
    } = this.props;

    const className = classNames('cursor fa fa-2x', {
      'fa-toggle-on text-success': isChecked,
      'fa-toggle-off text-danger': !isChecked
    });

    return (
      <span onClick={() => {
        this.setState({
          isChecked: !isChecked
        });
        if(onClick) {
          return onClick(!isChecked);
        }
      }}
      >
        <i className={className}></i>
      </span>
    );
  }
}

module.exports = Toggle;
