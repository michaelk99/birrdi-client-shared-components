import React from 'react';

import Dropdown from '@shared/Dropdown';
import Checkbox from '@shared/elements/Checkbox';

import find from 'lodash/find';

function GolfCourseDropdownMultipleNav(props) {
  const {
    activeGolfCourses,
    golfCourses,
    setToastification,
    updateNavGolfCourses
  } = props;

  if(!golfCourses.length) {
    return (
      <div className="c-phantom-picker"></div>
    );
  }

  const options = golfCourses.map(gc => {
    const isChecked = find(activeGolfCourses, { id : gc.id });
    let text = isChecked ? 'Removed' : 'Added';
    return (
      <li
        className="text-primary"
        key={`li-multi-nav-${gc.id}`}
        onClick={e => {
          // bad entry -- Update toast
          if(activeGolfCourses.length === 1) {
            text = 'Cannot remove';
          }
          setToastification({
            uuid: `golfCourse-id-${gc.id}`,
            message: `${text} ${gc.name}`
          });
          updateNavGolfCourses(gc);
          // keep the dropdown open on click to make it less jarring
          e.preventDefault();
          e.stopPropagation();
        }}
      >
        <Checkbox
          checked={isChecked}
          onChange={e => {
            // bad entry -- Update toast
            if(activeGolfCourses.length === 1) {
              text = 'Cannot remove';
            }
            setToastification({
              uuid: `golfCourse-id-${gc.id}`,
              message: `${text} ${gc.name}`
            });
            updateNavGolfCourses(gc);
            e.preventDefault();
            e.stopPropagation();
          }}
        >
        </Checkbox>
        <div>{gc.name}</div>
      </li>
    );
  });

  let toggleName = activeGolfCourses.map(ac => ac.name).join(', ');

  if(activeGolfCourses.length > 1) {
    toggleName = 'Viewing multiple courses';
  }

  return (
    <div className="text-center m-t-1">
      <div className="default-input d-inline-block">
        <Dropdown
          customToggle={toggleName}
        >
          {options}
        </Dropdown>
      </div>
    </div>
  );
}

module.exports = GolfCourseDropdownMultipleNav;
