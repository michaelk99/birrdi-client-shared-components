import React from 'react';
import { Link } from 'react-router-dom';

import Dropdown from '@shared/Dropdown';

function GolfCoursePicker(props) {
  const {
    setActiveGolfCourse,
    activeGolfCourse,
    golfCourses
  } = props;

  if(!golfCourses.length) {
    return (
      <div className="p-a-3 text-center">
        <Link to="/setup">
          <span className="text-primary dropdown-border">
            Create a golf course&nbsp;&nbsp;
            <i className="fas fa-long-arrow-alt-right"></i>
          </span>
        </Link>
      </div>
    );
  }

  const options = golfCourses.map(gc => {
    return (
      <li
        className="text-primary"
        key={`gc-picker-${gc.id}`}
        onClick={() => setActiveGolfCourse(gc)}
      >
        <div>{gc.name}</div>
      </li>
    );
  });

  if(!activeGolfCourse) {
    return (
      <div className="text-center text-primary p-a-3">
        <div className="d-inline-block dropdown-border">
          <Dropdown customToggle="Select a golf course">
            {options}
          </Dropdown>
        </div>
      </div>
    );
  }

  return (
    <div className="text-center">
      <div className="d-inline-block">
        <Dropdown customToggle={`${activeGolfCourse.name}`}>
          {options}
        </Dropdown>
      </div>
    </div>
  );
}

module.exports = GolfCoursePicker;
