import React from 'react';

import Dropdown from '@shared/Dropdown';

function GolfCoursePickerNav(props) {
  const {
    activeGolfCourse,
    golfCourses,
    setActiveGolfCourse,
    setToastification
  } = props;

  if(!golfCourses.length) {
    return (
      <div className="c-phantom-picker"></div>
    );
  }

  if(!activeGolfCourse) {
    return (
      <div className="c-phantom-picker"></div>
    );
  }

  const options = golfCourses.map(gc => {
    return (
      <li
        className="text-primary"
        key={`gc-picker-${gc.id}`}
        onClick={() => {
          setActiveGolfCourse(gc);
          setToastification({
            uuid: `golfCourse-id-${gc.id}`,
            message: `Switched to ${gc.name}`
          });
        }}
      >
        <div>{gc.name}</div>
      </li>
    );
  });

  return (
    <div
      className="text-center"
      style={{
        marginTop: '.5rem'
      }}
    >
      <div className="default-input d-inline-block">
        <Dropdown customToggle={`${activeGolfCourse.name}`}>
          {options}
        </Dropdown>
      </div>
    </div>
  );
}

module.exports = GolfCoursePickerNav;
