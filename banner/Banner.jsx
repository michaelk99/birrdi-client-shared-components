import React from 'react';

import classNames from 'classnames';

function Banner(props) {
  const {
    children,
    lg,
    md,
    primary,
    secondary,
    sm
  } = props;

  const bannerClassName = classNames('');

  return (
    <div className={bannerClassName}>
      {children}
    </div>
  );
}

module.exports = Banner;
