import React from 'react';

import copy from 'copy-to-clipboard';
// import uuid from 'uuid';

function CopyField(props) {
  const {
    text,
    copyData,
    setToastification
  } = props;

  return (
    <div
      className="copy-field"
      onClick={() => {
        copy(copyData);

        if(setToastification) {
          setToastification({
            uuid: Math.random(),
            message: 'Copied to clipboard'
          });
        }
      }}
    >
      {text}
    </div>
  );
}

module.exports = CopyField;
