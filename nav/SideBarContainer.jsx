'';

import React from 'react';

function SideBarContainer({ children }) {
  return (
    <div className="sidebar hidden-sm-down">
      {children}
    </div>
  );
}

module.exports = SideBarContainer;
