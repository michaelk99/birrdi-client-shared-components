'';

import React from 'react';

function SideBarNav({ children }) {
  return (
    <div className="sidebar-nav">
      <ul className="nav nav-sidebar" id="sideBarId">
        {children}
      </ul>
    </div>
  );
}

module.exports = SideBarNav;
