import React from 'react';
import { Link } from 'react-router-dom';

function PermissionDenied() {
  const message = 'You do not have privileges to view this page';
  return (
    <div className="container-fluid">
      <div className="container-content">
        <div className="card card-shadow">
          <div className="card-block">
            <div className="jumbotron text-center">
              <h2 className="display-3">Sorry :/</h2>
              <div>{message}</div>
              <hr className="m-y-2"></hr>
              <Link className="btn-link cursor btn-lg" to="/">
                <div>Go Back</div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
module.exports = PermissionDenied;
