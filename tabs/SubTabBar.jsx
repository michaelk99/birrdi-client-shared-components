import React from 'react';
import SubTab from './SubTab';

function SubTabBar(props) {
  const {
    subTabs,
    activeId,
    includeDate,
    responsive
  } = props;

  const _subTabs = subTabs.map((t, i) => {
    return (
      <SubTab
        activeId={activeId}
        className={t.className}
        id={t.id}
        includeDate={includeDate}
        key={i}
        pathname={t.pathname}
        query={t.query}
        responsive={responsive}
        tabText={t.tabText}
        tabTextResponsive={t.tabTextResponsive}
      />
    );
  });

  return (
    <div className="sub-tab-bar">
      {_subTabs}
    </div>
  );
}

module.exports = SubTabBar;
