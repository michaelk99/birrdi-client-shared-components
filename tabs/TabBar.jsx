import React from 'react';
import Tab from './Tab';

function TabBar(props) {
  const {
    tabs,
    activeId,
    className = 'tab-bar',
    children
  } = props;

  const _tabs = tabs.map((t, i) => {
    return (
      <Tab
        activeId={activeId}
        className={t.className}
        id={t.id}
        key={i}
        pathname={t.pathname}
        query={t.query}
        tabText={t.tabText}
      />
    );
  });

  return (
    <div
      className={className}
    >
      {_tabs}
      {children}
    </div>
  );
}

module.exports = TabBar;
