import React from 'react';
import { Link } from 'react-router-dom';

import classNames from 'classnames';
import moment from 'moment';
import queryString from 'query-string';

const includeDateMap = {
  Monday: 1,
  Tuesday: 2,
  Wednesday: 3,
  Thursday: 4
};

function SubTabWithDate(props) {
  const {
    id,
    activeId,
    pathname,
    query,
    className = '',
    responsive
  } = props;

  let {
    tabText,
    tabTextResponsive
  } = props;

  const tabClassName = classNames('nav-item nav-link', {
    'active': id === activeId
  });

  const today = moment();
  const todayDow = today.day();
  const tabDow = includeDateMap[tabText];
  const numDays = Math.abs(todayDow - tabDow);

  if(todayDow === 0 || todayDow > 4) {
    today.add('7', 'days');
  }

  const dateFormatted = (
    todayDow > tabDow ?
    today.subtract(numDays, 'day').format('MM/DD') :
    today.add(numDays, 'day').format('MM/DD')
  );

  tabText += ' ' + dateFormatted;
  tabTextResponsive += ' ' + dateFormatted;

  return (
    <div className={'nav-item ' + tabClassName}>
      <Link
        className={className + ' hidden-xs-down'}
        to={{ pathname, query }}
      >
        <div>{tabText}</div>
      </Link>
      <Link
        className={className + ' hidden-sm-up'}
        to={{
          pathname,
          search: queryString.stringify(query)
        }}
      >
        <div>{responsive ? tabTextResponsive : tabText}</div>
      </Link>
    </div>
  );
}

module.exports = SubTabWithDate;
