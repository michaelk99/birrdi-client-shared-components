import React from 'react';
import { Link } from 'react-router-dom';

import classNames from 'classnames';
import queryString from 'query-string';

import SubTabWithDate from './SubTabWithDate';

function SubTab(props) {
  const {
    tabText,
    id,
    activeId,
    pathname,
    query,
    className,
    includeDate,
    responsive,
    tabTextResponsive
  } = props;

  if(includeDate) {
    return (
      <SubTabWithDate
        activeId={activeId}
        className={className}
        id={id}
        pathname={pathname}
        query={query}
        responsive={responsive}
        tabText={tabText}
        tabTextResponsive={tabTextResponsive}
      />
    );
  }

  const tabClassName = classNames('nav-item nav-link', {
    'active': id === activeId
  });

  return (
    <div className={tabClassName}>
      <Link
        className="hidden-xs-down"
        to={{
          pathname,
          search: queryString.stringify(query)
        }}
      >
        <div>{tabText}</div>
      </Link>
      <Link
        className="hidden-sm-up"
        to={{
          pathname,
          search: queryString.stringify(query)
        }}
      >
        <div>
          {responsive && tabTextResponsive ? tabTextResponsive : tabText}
        </div>
      </Link>
    </div>
  );
}

module.exports = SubTab;
