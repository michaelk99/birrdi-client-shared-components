import React from 'react';
import { Link } from 'react-router-dom';

import classNames from 'classnames';
import queryString from 'query-string';

function Tab(props) {
  const {
    tabText,
    id,
    activeId,
    pathname,
    query,
    className = ''
  } = props;

  const tabClassName = classNames('nav-item nav-link', {
    'active': id === activeId
  });

  return (
    <div className={tabClassName}>
      <Link
        className={className}
        to={{
          pathname,
          search: queryString.stringify(query)
        }}
      >
        <div>{tabText}</div>
      </Link>
    </div>
  );
}

module.exports = Tab;
