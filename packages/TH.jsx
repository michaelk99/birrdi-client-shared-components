'';

import React from 'react';

function TH({ children, className = '' }) {
  return (
    <th className={className}>
      {children}
    </th>
  );
}

module.exports = TH;
