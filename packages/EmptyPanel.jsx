import React from 'react';

import PanelContainer from '@shared/panel/PanelContainer';
import PanelBody from '@shared/panel/PanelBody';
import PanelHeader from '@shared/panel/PanelHeader';

function EmptyPanel({ header, body, children }) {
  return (
    <PanelContainer>
      <PanelHeader>
        {header}
      </PanelHeader>
      <PanelBody>
        <div className="p-t-2 p-b-1 text-center">
          <h4>{body}</h4>
        </div>
        {children}
      </PanelBody>
    </PanelContainer>
  );
}

module.exports = EmptyPanel;
