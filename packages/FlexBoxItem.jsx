'';

import React from 'react';

function FlexBoxItem({ children, className = '', style }) {
  return (
    <div className={`flex-item ${className}`} style={style}>
      {children}
    </div>
  );
}

module.exports = FlexBoxItem;
