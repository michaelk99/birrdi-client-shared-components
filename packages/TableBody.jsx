'';

import React from 'react';

function TableBody({ children }) {
  return (
    <tbody>
      {children}
    </tbody>
  );
}

module.exports = TableBody;
