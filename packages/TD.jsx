import React from 'react';

function TD({ children, className = '', innerClassName = '' }) {
  return (
    <td className={`${className} default`}>
      {children}
    </td>
  );
}

module.exports = TD;
