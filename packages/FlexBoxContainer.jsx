'';

import React from 'react';

function FlexBoxContainer({ children }) {
  return (
    <div className="flex-box">
      {children}
    </div>
  );
}

module.exports = FlexBoxContainer;
