'';

import React from 'react';

function TableContainer({ children, className, responsive }) {
  if(responsive) {
    return (
      <div className="table-responsive">
        <table className={className ? className : 'table table-hover'}>
          {children}
        </table>
      </div>
    );
  }
  return (
    <table className={className ? className : 'table table-hover'}>
      {children}
    </table>
  );
}

module.exports = TableContainer;
