import React from 'react';

import moment from 'moment';
import map from 'lodash/map';
import range from 'lodash/range';

import TimelineCaret from './TimelineCaret';
import TimelineTab from './TimelineTab';

const TIMELINE_LENGTH = 7;
const TIMELINE_MAX_WEEK = moment().add('3', 'weeks');

function Timeline(props) {
  const {
    date,
    dayIndex,
    onTabChange,
    pastValidationFn
  } = props;

  const today = moment();
  let copyDate = moment(date);

  const tabs = map(range(TIMELINE_LENGTH), index => {
    const _tab = (
      <TimelineTab
        active={dayIndex === index}
        date={moment(copyDate)}
        index={index}
        key={index}
        onTabChange={onTabChange}
        originalDate={date}
        tabDate={copyDate.format('MM/DD')}
        tabDow={copyDate.format('ddd')}
      />
    );
    copyDate = moment(copyDate).add('1', 'day');
    return _tab;
  });

  const defaultPastValidationFn = () => {
    return moment(date).subtract('7', 'days').isSameOrAfter(today, 'day');
  };

  const _pastValidationFn = pastValidationFn || defaultPastValidationFn;

  return (
    <div className="timeline-horizontal">
      <ul className="nav nav-tabs d-flex c-center">
        <li
          className="cursor today-btn hidden-xs-down"
          onClick={() => onTabChange({
            date: moment(),
            dayIndex: 0
          })}
        >
          <div>Today</div>
        </li>
        <TimelineCaret
          className="timeline-caret-left cursor"
          goToDate={moment(date).subtract('1', 'week')}
          iconClassName="fa fa-chevron-left"
          onTabChange={onTabChange}
          validationFn={_pastValidationFn}
        />
        {tabs}
        <TimelineCaret
          className="timeline-caret-right cursor"
          goToDate={moment(date).add('1', 'week')}
          iconClassName="fa fa-chevron-right"
          onTabChange={onTabChange}
          validationFn={() => {
            return moment(date)
            .add('7', 'days')
            .isBefore(TIMELINE_MAX_WEEK, 'day');
          }}
        />
      </ul>
    </div>
  );
}

module.exports = Timeline;
