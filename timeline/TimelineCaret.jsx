import React from 'react';

function TimelineCaret({
  goToDate,
  className,
  iconClassName,
  dayIndex,
  validationFn = () => false,
  onTabChange
}) {
  if(!validationFn()) {
    return null;
  }
  return (
    <li
      className={className}
      onClick={() => onTabChange({
        dayIndex: dayIndex || 0,
        date: goToDate,
        weekChange: true
      })}
    >
      <div><i className={iconClassName}></i></div>
    </li>
  );
}

module.exports = TimelineCaret;
