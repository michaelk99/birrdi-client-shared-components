import React from 'react';

function TimelineTab({
  index,
  tabDow,
  tabDate,
  active,
  originalDate,
  onTabChange
}) {
  let className = active ? 'nav-link active' : 'nav-link';
  return (
    <li className="nav-item cursor">
      <a
        className={className}
        onClick={() => onTabChange({
          dayIndex: index,
          date: originalDate
        })}
      >
        <b className="tab-dow">{tabDow}</b>
        <b className="tab-date">{tabDate}</b>
      </a>
    </li>
  );
}

module.exports = TimelineTab;
